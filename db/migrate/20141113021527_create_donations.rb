class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.date :date
      t.integer :donor_id
      t.integer :recipient_id
      t.decimal :total

      t.timestamps
    end
  end
end
