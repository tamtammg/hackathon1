class CreateRecipients < ActiveRecord::Migration
  def change
    create_table :recipients do |t|
      t.integer :recipient_id
      t.string :last_name
      t.string :first_name
      t.string :address

      t.timestamps
    end
  end
end
