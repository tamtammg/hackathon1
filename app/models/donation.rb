class Donation < ActiveRecord::Base

  belongs_to :donor
  belongs_to :recipient

  def self.donation_total
    Donation.sum(:total) || 0.00
  end

end
