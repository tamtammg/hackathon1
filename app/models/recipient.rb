class Recipient < ActiveRecord::Base

  has_many :donors, through: :donations
  has_many :donations

end
